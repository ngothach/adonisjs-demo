## Instruction
1. Use ```npm install``` to install all the required packages
2. Run ```mkdir tmp``` to make temporary director
3. Create ```.env``` file, you could run ```cp .env.example .env``` for quick start
4. Run ```npm test``` to run unit test or ```node ace serve --watch``` to start development server
5. To deploy the project, please follow instruction here: https://docs.adonisjs.com/guides/deployment