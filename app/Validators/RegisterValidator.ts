import { rules, schema, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CustomReporter from './Reporters/CustomReporter'

export default class RegisterValidator {
  constructor(protected ctx: HttpContextContract) {}

  public reporter = CustomReporter

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    firstname: schema.string(),
    lastname: schema.string(),
    email: schema.string({}, [
       rules.email(),
       rules.unique({
         table: 'users',
         column: 'email',
       }),
    ]),
    password: schema.string({ }, [
      rules.password()
    ]),
    phone: schema.string.nullableAndOptional(),
    avatar: schema.string.nullableAndOptional(),
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages: CustomMessages = {
    'firstname.required'     : '40000|First name is required',
    'firstname.string'       : '40000|First name must be string',
    'lastname.required'      : '40000|Last name is required',
    'lastname.string'        : '40000|Last name must be string',
    'email.unique'           : '40001|Email already exists',    
    'email.required'         : '40002|Email is required',
    'email.string'           : '40002|Email wrong format',
    'email.email'            : '40002|Email wrong format',
    'password.required'      : '40003|Password is required',
    'password.string'        : '40003|Password wrong format',
    'password.passwordFormat': '40003|Password wrong format'
  }
}
