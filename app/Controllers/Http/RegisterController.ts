import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { RegisterValidator } from 'App/Validators'
import User from 'App/Models/User'

export default class RegisterController {
  
  async store({ auth, request }: HttpContextContract) {
    const register = await request.validate(RegisterValidator)
    const user = await User.create(register);
    await user.save()
    const accessToken = await auth.use('jwt').generate(user);

    return { code: 200, accessToken }
  }
}
