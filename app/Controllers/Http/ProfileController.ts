import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { UpdateProfileValidator } from 'App/Validators'

export default class ProfileController {

  async update({ auth, request }: HttpContextContract) {
    const profile = await request.validate(UpdateProfileValidator)
    await auth.user!.merge(profile).save()
    return { code: 200 }
  }
}
