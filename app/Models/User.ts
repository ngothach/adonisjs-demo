import Hash from '@ioc:Adonis/Core/Hash'
import { BaseModel, beforeSave, column } from '@ioc:Adonis/Lucid/Orm'

export default class User extends BaseModel {
  @column({ isPrimary: true, columnName: 'user_id' })
  public userID: number

  @column()
  public firstname: string

  @column()
  public lastname: string
  
  @column()
  public email: string

  @column()
  public password: string

  @column()
  public phone: string | null

  @column()
  public avatar: string | null

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }  
}
