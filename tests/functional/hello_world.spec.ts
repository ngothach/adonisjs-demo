import { test } from '@japa/runner'

const validAccount = {
  email: 'test@test.com',
  password: '123456789aA!'
}

let token = {}

test.group('Registration', () => {
  test('First name is required', async ({ client }) => {
    const response = await client.post('/register').form({ })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40000, message: 'First name is required' })
  })

  test('First name must be string (null)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: null })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40000, message: 'First name is required' })
  })

  test('First name must be string (empty)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: '' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40000, message: 'First name is required' })
  })

  test('Last name is required', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40000, message: 'Last name is required' })
  })

  test('Email is required', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40002, message: 'Email is required' })
  })

  test('Email wrong format (email: 11)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: '11' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40002, message: 'Email wrong format' })
  })    

  test('Email wrong format (email: 111@111.11111)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: '111@111.11111' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40002, message: 'Email wrong format' })
  })

  test('Password is required', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: 'test@test.com' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40003, message: 'Password is required' })
  })

  test('Password wrong format (< 8 character)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: 'test@test.com', password: '1' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40003, message: 'Password wrong format' })
  })
  test('Password wrong format (> 20 character)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: 'test@test.com', password: '123456789123456789123456789' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40003, message: 'Password wrong format' })
  })    

  test('Password wrong format (no alpha, no special char)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: 'test@test.com', password: '123456789' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40003, message: 'Password wrong format' })
  })
  
  test('Password wrong format (no special char)', async ({ client }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: 'test@test.com', password: '123456789aA' })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40003, message: 'Password wrong format' })
  })    

  test('Registration OK', async ({ client, assert }) => {
    const response = await client.post('/register').form({ firstname: 'A', lastname: 'Nguyen Van', email: validAccount.email, password: validAccount.password })
    response.assertStatus(200)
    assert.properties(response.body(), ['code', 'accessToken'])
  })      
})

test.group('Login', () => {

  test('Login with no email', async ({ client }) => {
    const response = await client.post('/login')
    response.assertStatus(422)
    response.assertBodyContains({ code: 40001, message: 'Email is required' })
  })

  test('Login with invalid email (wrong format)', async ({ client }) => {
    const response = await client.post('/login').form({
      email: 'test@test.co1m'
    })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40001, message: 'Email wrong format' })
  })  

  test('Login with no password', async ({ client }) => {
    const response = await client.post('/login').form({
      email: 'test@test.com'
    })
    response.assertStatus(422)
    response.assertBodyContains({ code: 40002, message: 'Password is required' })
  })

  test('Login with invalid account', async ({ client }) => {
    const response = await client.post('/login').form({
      email: 'test1@test.com',
      password: '11111111111111aA!'
    })
    response.assertStatus(401)
    response.assertBodyContains({ code: 401, message: 'Unauthorized' })
  })

  test('Login OK', async ({ client, assert }) => {
    const response = await client.post('/login').form(validAccount)
    response.assertStatus(200)
    assert.properties(response.body(), ['code', 'accessToken'])
    // Save for Profile test
    token = response.body().accessToken.token
  })      
})

test.group('Profile', () => {

  test('Update profile with no token', async({ client }) => {
    const response = await client.post('/profile').form({ firstname: 'B' })
    response.assertStatus(401)
  })

  test('Update profile OK', async({ client }) => {
    const response = await client.post('/profile').header('Authorization', `Bearer ${token}`).form({ firstname: 'B' })
    response.assertStatus(200)
  })  
})