import {
  ValidationException,
  MessagesBagContract,
  ErrorReporterContract,
} from '@ioc:Adonis/Core/Validator'

/**
 * The shape of an individual error
 */
type ErrorNode = {
  message: string,
  code: any,
}

export default class CustomReporter implements ErrorReporterContract<ErrorNode> {
  public hasErrors = false

  /**
   * Tracking reported errors
   */
  private error: ErrorNode

  constructor(
    private messages: MessagesBagContract,
    private bail: boolean,
  ) {
  }

  /**
   * Invoked by the validation rules to
   * report the error
   */
  public report(
    pointer: string,
    rule: string,
    message: string,
    arrayExpressionPointer?: string,
    args?: any
  ) {
    /**
     * Turn on the flag
     */
    this.hasErrors = true

    /**
     * Use messages bag to get the error message. The messages
     * bag also checks for the user-defined error messages and
     * hence must always be used
     */
    const errorMessage = this.messages.get(
      pointer,
      rule,
      message,
      arrayExpressionPointer,
      args,
    )
    const i = errorMessage.indexOf('|')
    if (i > 0) {
      /**
       * Track error message
       */
      this.error = { code: Number.parseInt(errorMessage.substring(0, i)), message: errorMessage.substring(i + 1) }

      /**
       * Bail mode means stop validation on the first
       * error itself
       */
      if (this.bail) {
        throw this.toError()
      }
    } else {
      throw new ValidationException(false, { message: 'Error message wrong format' })
    }
  }

  /**
   * Converts validation failures to an exception
   */
  public toError() {
    throw new ValidationException(false, this.toJSON())
  }

  /**
   * Get error messages as JSON
   */
  public toJSON() {
    return this.error
  }
}
