import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { AuthenticationException } from '@adonisjs/auth/build/standalone'
import { LoginValidator } from 'App/Validators'

export default class AuthenticationController {
  
  async login({ auth, request }: HttpContextContract) {
    const login = await request.validate(LoginValidator)
    try {
      const accessToken = await auth.use('jwt').attempt(login.email, login.password)
      return { code: 200, accessToken }
    } catch {
      throw new AuthenticationException('Unauthorized', 'E_UNAUTHORIZED_ACCESS')
    }
  }
}
