import { validator } from '@ioc:Adonis/Core/Validator'

validator.rule('password', (value, _, options) => {
  if (typeof value !== 'string'
    || (value.length < 8 || value.length > 20)
    || (/[a-z]/.test(value) === false)
    || (/[A-Z]/.test(value) === false)
    || (/\d/.test(value) === false)
    || (/[~!@#\$%\^&\*\(\)\-\_\+=]/.test(value) === false)
  ) {
    options.errorReporter.report(
      options.pointer,
      'passwordFormat',
      'Password wrong format',
      options.arrayExpressionPointer,
    )
  }
})
